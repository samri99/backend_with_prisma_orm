const router = require("express").Router();
const userController = require("../controllers/user.controllers");
const user = require("../validators/payloadValidation");
const auth = require("../authControllers/user.auth");


  function authorize(req, res, next){
   if(!req.header('Authorization')){
      res.status(401).json({
        message: "You need to login first."
      })
    }else{
      require("../authControllers/passport").authenticate("jwt",{ session: false }, (err, token) => {
   
        if(err){
         console.log("Problem with auth")
        } 
        else {
           if(token){
            console.log(token)
            req.user = token.user
            next();
           }
           else{
            res.status(403).json({
               message:"You need to log in first."
            })
            console.log("the token " + token); 
           }
         
        }
         
     })
     (req, res, next);
   }
}
router.get('/', (req, res,)=>{
   res.status(404).json({
      message: "message"
   })
   console.log(res.statusCode)
});
router.post("/users", user.userValidator, userController.registerUser);
router.post("/users/teacher/:courseId", user.userValidator, authorize, auth.isTeacherOfCourseOrIsAdmin, userController.createTeacherUser )
router.get("/users", authorize, auth.isTeacherOfCourseOrIsAdmin, userController.viewAll);
router.get("/users/:userId", user.userParamsValidator, userController.userbyId);


//As a student get what courses she has enrolled in wants to enrol
router.get('/testResults', authorize, userController.getTestResult)


router.get('/myCourses', auth.authorize, user.paramsValidator, userController.getCourseOfStudent)
router.post('/enrolCourse/:courseId', authorize,user.paramsValidator, userController.enrollCourse)


router.post("/login",user.userEmailValidator, auth.loginHandler);
router.post("/authenticate/:emailToken",user.emailTokenValidator, user.userParamsValidator, auth.authenticateHandler);

module.exports = router;
