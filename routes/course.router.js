const router = require("express").Router();
const joi = require('joi')
const  payloadValidation  = require('../validators/payloadValidation');
const courseController = require('../controllers/course.controller');
const auth = require('../authControllers/user.auth')



router.post('/courses', auth.authorize, courseController.createCoursesAndUsers )

router.post('/testresult/:testId',auth.authorize, auth.isTeacherOfCourseOrIsAdmin, payloadValidation.paramsValidator, payloadValidation.courseBodyValidator,  courseController.createTestResult)
router.put('/testresult/:testId',auth.authorize, auth.isTeacherOfCourseOrIsAdmin,payloadValidation.paramsValidator, payloadValidation.courseBodyValidator, courseController.updateTestResult)
router.get('/alltestreults', auth.authorize, auth.isTeacherOfCourseOrIsAdmin, courseController.viewAllTestResults)


router.post('/createTest/:courseId', auth.authorize, auth.isTeacherOfCourseOrIsAdmin,payloadValidation.courseBodyValidator, payloadValidation.paramsValidator, courseController.createTest)
router.get('/alltests', auth.authorize, auth.isTeacherOfCourseOrIsAdmin, courseController.viewAllTests)


router.get('/courses', auth.authorize, courseController.viewAllCourses)
router.post('/registerCourse',auth.authorize, payloadValidation.coursesValidator, courseController.registerCourse);
router.put('/updateCourse/:courseId', auth.authorize,auth.isTeacherOfCourseOrIsAdmin,payloadValidation.courseBodyValidator, payloadValidation.paramsValidator, courseController.updateCourse)
router.delete('/deleteCourse/:courseId', auth.authorize,auth.isTeacherOfCourseOrIsAdmin, payloadValidation.courseBodyValidator, payloadValidation.paramsValidator, courseController.deleteCourse)
module.exports =  router