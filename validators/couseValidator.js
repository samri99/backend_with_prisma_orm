const joi = require('joi')

const courseSchema = joi.object({
    name: joi.string().required(),
    coursedetails: joi.string().required(),
    
    tests:{
        name: joi.string().optional()
    }

})
const paramsValidation = joi.object({
    courseId: joi.number(),
    testId : joi.string().regex(RegExp(/[0-9]+$/)),
    result: joi.string().regex(RegExp(/[0-9]+$/)), 
    email: joi.string().email()
})
const coursebodyValidation = joi.object({
    name: joi.string(),
    coursedetails: joi.string(),
    result: joi.number(), 
    email: joi.string().email()
})
module.exports ={
    courseSchema,
    paramsValidation,
    coursebodyValidation
}