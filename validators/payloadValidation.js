const userValidator = require('./user.validator')
const coursesValidator = require('./couseValidator')
exports.userValidator = async (req, res, next) =>{

    const isError = await userValidator.validatorSchema.validate(req.body, {abortEarly: false})
    if (isError.error){
        res.status(422).json({
            status: "failed",
            data: isError
        })
    }
    else{
        next();
    }

}
exports.courseBodyValidator = async (req, res, next) =>{

    const isError = await coursesValidator.coursebodyValidation.validate(req.body, {abortEarly: false})
    if (isError.error){
        res.status(422).json({
            status: "failed",
            data: isError
        })
    }
    else{
        next();
    }

}
exports.userParamsValidator =async (req, res, next) =>{
    const isError = await userValidator.paramsValidator.validate(req.params, {abortEarly: false})
    if (isError.error){
        res.status(400).json({
            status: "failed",
            data: isError.error.details
        })
    }
    else{
        next();
    }

}

exports.coursesValidator = async (req, res, next) =>{
    const isError = await coursesValidator.courseSchema.validate(req.body, {abortEarly: false})
    if (isError.error){
        res.status(400).json({
            status: "failed",
            data: isError.error.details
        })
    }
    else{
        next();
    }
}
exports.paramsValidator = async (req, res, next) =>{
    const isError = await coursesValidator.paramsValidation.validate(req.params, {abortEarly: false})
    if (isError.error){
        res.status(400).json({
            status: "failed",
            data: isError.error.details
        })
    }
    else{
        next();
    }
}
exports.userEmailValidator = async (req, res, next) =>{
    const isError = await coursesValidator.paramsValidation.validate( req.body, {abortEarly: false})
    if (isError.error){
        res.status(400).json({
            status: "failed",
            data: isError.error.details
        })
    }
    else{
        next();
    }
}
exports.emailTokenValidator = async (req, res, next) =>{
    const isError = await userValidator.emailTokenValidation.validate(req.params, {abortEarly: false})
    if (isError.error){
        res.status(400).json({
            status: "failed",
            data: isError.error.details
        })
    }
    else{
        next();
    }
}