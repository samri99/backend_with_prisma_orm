const joi = require('joi');

const validatorSchema = joi.object({
    firstname: joi.string().required().regex(RegExp(/^[A-Za-z]+/)).messages({
        'string/base':'input needs to be string only'
    }),
    lastname: joi.string().required().regex(RegExp(/^[A-Za-z]+/)).messages({
        'string/base':'input needs to be string only'
    }),
    email: joi.string().required().email(),
    social:{
        facebook: joi.string().optional(),
        twitter:joi.string().optional()
    }
})

const paramsValidator = joi.object({
    userId: joi.string().regex(RegExp(/[0-9]+$/)),
   emailToken: joi.string().regex(RegExp(/[0-9]+$/)),
   email: joi.string().email()

})
const apiValidationSchema = joi.object({
    id: joi.number().integer().required(),
    iat: joi.any()
})
const emailTokenValidation = joi.object({
    emailToken: joi.string().regex(RegExp(/^[0-9]+$/)).required()
})

module.exports = {
    validatorSchema,
    paramsValidator,
    emailTokenValidation
} 

