const express = require("express");
const bodyparser = require("body-parser");
const app = express();
const dotenv  = require('dotenv')
const httpHandler = require("http-errors");
const PORT = process.env.PORT || 8888;
const HOST = process.env.HOST
const courseRouters = require("./routes/course.router");
const userRouters = require("./routes/user.router");
 const passport = require('./authControllers/passport')

dotenv.config();
/*app.use((req, res, next)=>{
    next(httpHandler.NotFound())
})*/
const errorHendler = (err, req, res, next) => {
  
  if (err) {
   next( httpHandler[404])
  } else {
    next();
  }
};
app.use(errorHendler)
app.use(passport.initialize());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());
app.use(courseRouters);
app.use(userRouters);


app.listen(PORT, () => {    console.log(` SErver running http://${HOST}:${PORT}`);
  console.log(process.env.PORT)
});

module.exports = app;
