const passport = require ('passport')
const JWTExtract = require('passport-jwt').ExtractJwt
const JWTStrategy = require('passport-jwt').Strategy
const {PrismaClient, TokenType, Prisma } = require('@prisma/client')
const {user, token, course } = new PrismaClient();

var opt = {}
opt.jwtFromRequest  = JWTExtract.fromAuthHeaderAsBearerToken();
opt.secretOrKey = process.env.JWT_SECRET;
opt.algorithm = process.env.ALGORITHM
console.log("this is from the passport");
console.log(JSON.stringify(opt.jwtFromRequest))

module.exports = passport.use(new JWTStrategy(opt, async (jwt_payload, done) => {
    console.log(JSON.stringify(opt.jwtFromRequest))
    console.log(jwt_payload)
    const foundToken = await token.findUnique({
        where:{
            id: jwt_payload.id
        },
        include: {
            user: true
        }
    })
        if (foundToken){
            return  done ( null, foundToken)
        }
        else{
            return done (null, {message: "token not found"})

        }
    }))
    