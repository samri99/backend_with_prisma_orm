const jwt = require("jsonwebtoken");
const { PrismaClient, TokenType, Prisma, UserRole } = require("@prisma/client");
var add = require("date-fns/add");
const { user, token, course } = new PrismaClient();
const JWTStrategy = require("passport-jwt").Strategy;
const JWTExtract = require("passport-jwt").ExtractJwt;
const passport = require("passport");

const expiration = add(new Date(), { minutes: 10 });
const apiExpiration = add(new Date(), { hours: 12 });

console.log(new Date());
console.log(expiration);
if (expiration < new Date()) {
  console.log("2 mins");
}

exports.authorization = async (req, res, next) => {
  console.log("header " + JSON.stringify(req.headers));
  if (
    req.headers.authorization == null &&
    req.headers.authorization.split(" ")
  ) {
    res.status(401).json({
      message: "Unauthorized user.",
    });
  } else {
    var opt = {};
    opt.jwtFromRequest = JWTExtract.fromAuthHeaderAsBearerToken();
    opt.secretOrKey = process.env.JWT_SECRET;
    opt.algorithm = process.env.ALGORITHM;

    console.log(JSON.stringify(opt));

    passport.use(
      new JWTStrategy(opt, async (payload, done) => {
        console.log(payload);
        await token.findUnique(
          {
            where: {
              id: payload.sub,
            },
            include: {
              user: true,
            },
          },
          (err, foundToken) => {
            console.log(foundToken);
            if (err) {
              console.log("error");
            } else {
              const findUser = user.findUnique({
                where: {
                  id: foundToken.user.id,
                },
              });

              res.status(200).json({
                message: "user Info",
                user: findUser,
              });
            }
          }
        );
        done();
      })
    );
  }
};

function generateEmailToken() {
  return Math.floor(10000000 * Math.random() * 90000000).toString();
}
function generateAPIToken(tokenId) {
  const payload = { id: tokenId };
  return jwt.sign({ id: tokenId }, process.env.JWT_SECRET);
}

exports.loginHandler = async (req, res, next) => {
  console.log("comming");

    try{
  const newtoken = await token.create({
    data: {
      emailToken: generateEmailToken(),
      type: TokenType.EMAIL,
      expiration: expiration,
      user: {
        connectOrCreate: {
          create: {
            email: req.body.email,
          },
          where: {
            email: req.body.email,
          },
        },
      },
    },
    include: {
      user: true,
    },
  })
  res.status(200).json({
    message: "created email token",
    token: newtoken,
  });
}catch(e){
   res.status(500).json({
    message: "unable to create token."
   })
 }
};

exports.auth = async (req, res, next) => {};

exports.authenticateHandler = async (req, res, next) => {
  console.log("the email token " + req.params.emailToken);
  try {
    const tokennn = await token.findUnique({
      where: {
        emailToken: req.params.emailToken,
      },
      include: {
        user: true,
      },
    });
    console.log("token " + JSON.stringify(tokennn.user.email));
    console.log("token " + (req.body.email));
    if (!tokennn.valid) {
      return res.status(400).json({
        message: "invalid Token.",
      });
    }
    if (tokennn.expiration < new Date()) {
      console.log(tokennn.expiration);
      console.log(new Date());
      return res.status(403).json({
        message: "Token Expired.",
      });
    }
    if (tokennn && tokennn.user.email == req.body.email) {

      console.log("api exp date " + apiExpiration);

      const createdToken = await token.create({
        data: {
          type: TokenType.API,
          expiration: apiExpiration,
          user: {
            connect: {
              email: tokennn.user.email,
            },
          },
        },
      });
      console.log("created Token " + createdToken.id);
      await token.update({
        where: {
          id: tokennn.id,
        },
        data: {
          valid: false,
        },
      });

      const authToken = generateAPIToken(createdToken.id);
      console.log(authToken);
       return res
        .status(200)
        .header("Authorization", "Bearer " + authToken)
        .json({
          message: "token created",
          token: authToken,
        });
    }
    else{
      res.json({
        message: "API token could not be generated for some reason"
      })
      console.log("With out the whole exceptions")
    }
  } catch (e) {
    console.log("From the catch")
    console.log(e);
  }
};

exports.authorize = (req, res, next) => {
  if(!req.header('Authorization')){
    res.status(401).json({
      message: "You need to login first."
    })
  }else{
  require("../authControllers/passport").authenticate(
    "jwt",
    { session: false },
    (err, token) => {
      if (err) {
        res.status(403).json({
          message: "Invalid token.",
        });
        console.log(err);
      } else {
        if (token) {
          console.log("this is the API token ");
          console.log(token);
          req.user = token.user;
          next();
        } 
        if(!token) {
           res.status(403).json({
            message: "Invalid token",
          });
          console.log("jjfjhfjhufhrejnvbdnvdbns");
          console.log("the token is " +  typeof(token));
         // next();
        }
      }
    }
  )(req, res, next);
  }
};
exports.isTeacherOfCourseOrIsAdmin =async  (req, res, next)=>{
  console.log("this is from authorization") 
  console.log(req.user)

    if(req.user.isAdmin){
        next();
    }
    else {
        const userFind = await user.findUnique({
            where:{
                id: req.user.id
            },
            include:{
                courses: true
            }
        })
        console.log(userFind)
        if(userFind.courses[0].role == UserRole.TEACHER){
            next();
        }
        else{
            res.status(403).json({
                message: "You are not allowed for this Action."
            })
        }
    }
}
exports.checkTokenExp = async( )=>{}