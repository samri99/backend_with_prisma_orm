
const app = require('../server')
const apprequest = require('../start');
const supertest = require('supertest');
const { PrismaClient, Prisma, TokenType, UserRole } = require("@prisma/client");
const  { user, course, test, testResult } = new PrismaClient();
var add = require("date-fns/add");
const jwt = require("jsonwebtoken");


/*const createTestUser = async( req, res, next)=>{

    const testUser = await user.create({
        data: {
            email: "test@mail.com",
            tokens:{
                create: {
                    expiration: add(new Date(), {days: 2}),
                    type: TokenType.API
                }
            }
        }, 
        include:{
            tokens: true,
            courses: {
                where: UserRole.TEACHER
            }
        }
    } ,()=>{
        req.body = testUser
    })
    return jwt.sign({id: testUser.id}, process.env.JWT_SECRET);
    //req.header['Authorization']= token
//(req, res, next)
  //  next();
}

const token = createTestUser();*/
describe("given the courses detail", ()=>{
    test('/registreCourse returns 200', async()=> {
        const response = await  apprequest.post('/registerCourse').send({
            name: "my course",
            coursedetails: "courseDetails"
        })
        expect(response.statusCode).toBe(403);
    })
    test('/createTest ', async ()=>{
        const response = await apprequest.post(`/createTest/2`).send({
            name: "test 3"
        })
        expect(response.statusCode).toBe(403)
        expect(response.body).isEqual({})
    })
    test('/myCourses returns 401 when an unuthorized',async ()=>{
        const response = await apprequest.get('/myCourses')
        expect(response.statusCode).toBe(401)
    })

})