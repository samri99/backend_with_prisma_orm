const { PrismaClient, Prisma, TokenType, UserRole } = require("@prisma/client");
const  { user, course, test, testResult } = new PrismaClient();
module.exports.createTestUser = async( req, res, next)=>{

    const testUser = await user.create({
        data: {
            email: "test@mail.com",
            tokens:{
                create: {
                    expiration: add(new Date(), {days: 2}),
                    type: TokenType.API
                }
            }
        }, 
        include:{
            tokens: true,
            courses: {
                where: UserRole.TEACHER
            }
        }
    } ,()=>{
        req.body = testUser
    })
    return jwt.sign({id: testUser.id}, process.env.JWT_SECRET);
    //req.header['Authorization']= token
//(req, res, next)
  //  next();
}
