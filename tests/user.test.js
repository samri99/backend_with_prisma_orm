const sum = require('../dummyFunc')
const apprequest = require('../start');
const supertest = require('supertest');
//const token = require('./testUser')
//const token = createTestUser()

test(' 5 + 5 should be 10', () => {
    const result = sum.sum(5, 5);
    expect(result).toBe(10);
})

describe('user inputs', ()=>{

test("/users 403 response", async () => {
    const response = await apprequest.post('/users').send({
        firstname: "false",
        lastname:"kflfl",
        email: "em@mail.com"
    })

    expect(response.headers['content-type']).toEqual(expect.stringContaining("json"))
})
test("/users 403 response", async () => {
    const response = await apprequest.post('/users').send({
        firstname: "nmdnfdm",
        lastname:"kflfl",
        email: "em@mail.com"
    })

    expect(response.statusCode).toEqual(400)
})
test("/users input validation", async () => {
    const response = await apprequest.post('/users').send({
        firstname: 5,
        lastname:"kflfl",
        email: "em@mail.com"
    })
    expect(response.statusCode).toEqual(422)
});

/*test("/ post 404 response", async (done)=>{

    const response = await apprequest.get('/');
   // console.log(response.statusCode)
    expect(response.statusCode).toEqual(404)
    
})*/

test('/login should return 200 on new emails', async ()=>{
    const response = await apprequest.post('/login').send({
        email: "myemail@mail.com"
    })
    expect(response.statusCode).toBe(200);
})
test('/testResults should return 401 becouse it has got no token', async ()=> {
    const response = await apprequest.get('/testResults')
    expect(response.statusCode).toBe(401);

})
test('/testResults should return 403 -> validation error', async ()=> {
    const response = await apprequest.post('/testresult/r')

    expect(response.statusCode).toBe(401);
})
test('POST /courses should return 403 -> validation error', async ()=> {
    const response = await apprequest.post('/courses')

    expect(response.statusCode).toBe(401);
})
test('/users/:userId should return 200', async ()=> {
    const response = await apprequest.get('/users/2')
    expect(response.statusCode).toBe(200);
})
test('/authenticate/:emailToken should return invalid token 400', async ()=> {

    const response = await apprequest.post('/authenticate/ghhg')

    expect(response.statusCode).toBe(400);
})

/*test("Create /testResult should give 403 due to invalid token ", async()=>{
    const response = await apprequest.post('/testresult/2').set("Authorization", `${token}`).send({
      result: "jhjh",
      email: "mry@mail.com"
    })
    expect(response.statusCode).toBe(403);
   // expect(response.body).isEqual(any())  
  })*/
  test('/myCourses returns 401 when an unuthorized',async ()=>{
    const response = await apprequest.delete('/deleteCourse/3')
    expect(response.statusCode).toBe(401)
})
})