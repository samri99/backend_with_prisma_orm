const { PrismaClient, Prisma, UserRole } = require("@prisma/client");
const { json } = require("body-parser");
const  { user, course, test, testResult } = new PrismaClient();
const { ServerError } = require("../ErrorHandler/ServerError");

exports.registerUser = async (req, res, next) => {
  let newUser;
 try{   newUser = await user.create({
    data: req.body,
    include: {
      courses: true,
    },
  })
 }catch(e){
  if (e instanceof Prisma.PrismaClientKnownRequestError){
    if(e.code == "P2002"){
        res.status(400).json({
          status: 'Bad Request',
          message: "Email already used."
        })
      }
  }
  else{
    res.status(500).json({
      message: "Server error."
    })
  }
 }
  if (newUser) {
    res.json({
      status: "success",
      user: newUser
    });
  }
  /* else { 
    res.status(200).json({
      status: "success",
      user: newUser
    });
  }*/
  
};
exports.registrStudentUser = async (req, res, next)=>{
  const findUser = await user.findUnique({
    where: {
      email: req.body.email
    }
  })
  if (findUser){
    res.status(400).json({
      message: "User with the same email already exists."
    })
  }
  else{
    const newUser = await user.create({
      data: {
        email: req.body.email,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        social: {
          facebook: "facebook"
        },
        courses:{
          create: {
            role: 'STUDENT',
            courses: {
              connect:{
                id: 2
              }
            }
          }
        }
      }
    })
  }
}

exports.removeUsers = async (req, res, next) => {
  const removed = await user.deleteMany({});
  res.status(200).json({
    users: removed,
  });
};

exports.viewAll = async (req, res, next) => {
  const allUsers = await user.findMany({
    include: {
      courses: true,
      testResult: true,
    },
  });
  res.status(200).json({
    message: "All users",
    users: allUsers,
  })
};

exports.getCourseOfStudent = async (req, res, next) => {
  console.log("this is form the mycourse function")
  const courses = await course.findMany({
    where:{
      members:{
        every:{
          userId: parseInt(req.user.id)
            /*users: {
              id: parseInt(req.user.id)
            }*/
        
        }
      }
    }, include:{
      tests: true
    }
  })
  console.log(courses)
  const tests = await course.findMany({
    where:{
      tests:{
        every:{
          testResult:{
            every:{
              student:{
                id: parseInt(req.user.id)
              }
            }
          }
        }
      }
    },
    include:{
      tests: true
    }
  })
  res.status(200).json({
    courses: courses
  })
}
exports.userbyId = async (req, res, next)=>{
  const theUser = await user.findUnique({
    where: {
      id: parseInt(req.params.userId)
    },
    include: {
      courses: true,
      testResult: true, 
      graded: true
    }
  })
  if(theUser){
    res.status(200).json({
      user: theUser,
    })
  }
  else{
    res.status(404).json({
      message:"User Not found"
    })
  }
}
exports.getTestResult = async (req, res, next)=>{
 if (!req.user){
  res.status(403).json({
    message: " you need to be authenticated."
  })
 }
 else {
   const theUser = await user.findFirst({
    where:{
      id: req.user.id,
      courses:{
        every:{
          role: UserRole.TEACHER
        }
      }
    }
   })
   if(!theUser){
    const testResults = await testResult.findMany({
      where:{
        studentId: req.user.id
      },
      include:{
        test: true,
        grader: true
      }
    })
  
    res.status(200).json({
      message: "Test results.",
      testResult: testResults
    })
   }
   else{
  const testResults = await testResult.findMany({
    where:{
      studentId: req.user.id
    },
    include:{
      test: true,
      grader: true
    }
  })

  res.status(200).json({
    message: "Test results.",
    testResult: testResults
  })
}
}
}

exports.createTeacherUser = async (req, res, json)=> {
  const findUser = await user.findUnique({
    where: {
      email: req.body.email
    }
  })
  if (findUser){
    res.status(400).json({
      message: "User with the same email already exists."
    })
  }
  else{
    const newUser = await user.create({
      data: {
        email: req.body.email,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        courses:{
          create: {
            role: 'TEACHER',
            courses: {
              connect:{
                id: parseInt(req.params.courseId)
              }
            }
          }
        }
      }
    })
    res.status(200).json({
      status: "success",
      teacher: newUser
    })
  }
}

exports.enrollCourse = async( req, res, next )=> {
    const enrolCourse = await course.findUnique({
      where:{
        id: parseInt(req.params.courseId)
      }
    })
    if(enrolCourse){
      const enrolUser = await user.update({
        where:{
          id: req.user.id
        },
        data:{
          courses:{
            create:{
              role: "STUDENT",
              courseId: parseInt(req.params.courseId)
            }
          }

          }, 
          include:{
            courses:true
          }

      })
      res.json({
        user: enrolUser
      })
    }
    else{
      res.status(404).json({
        message: "course not found."
      })
    }
}


