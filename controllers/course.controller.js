const { PrismaClient , UserRole} = require("@prisma/client");
const { boolean } = require("joi");
const { user, course, test, testResult, courseEnrollment } = new PrismaClient();
const utils = require('../utils/utils.date')

exports.viewAllCourses = async (req, res, next)=>{
        const allCourses = await course.findMany({
          include: {
            tests: true
          }
        })
        if(allCourses){
        res.status(200).json({
          courses: allCourses
        })
      }
      else{
        res.json({
          message: "Unable to fetch courses"
        })
      }
}

exports.registerCourse = async (req, res, next) => {
  const date = new Date().toUTCString();
  /*const newUser = await user.create({
        data: req.body,
      })s;*/

  const registeredCourse = await course.create({

    data: {
      name: req.body.name,
      coursedetails: req.body.coursedetails,
      tests: {
        create: [
          {
            name: "test 1"
          },
          {
            name: "test 2",
          },
        ],
      },
      members: {
        create: 
          [
            {
          role: "TEACHER",
          users: {
            connect: {
              id: req.user.id,
            },
          },
       },
      ],
      },
    },
    include:{
        tests: true
    },
  });
  if(!registeredCourse){
    res.status(500).json({
        message : "Course could not be registered."
    })
  }
  else{
    res.status(200).json({
        course: registeredCourse
    })
  }
};

exports.updateCourse = async (req, res, next)=>{
  const updatedCourse = await course.update({
    where: {
      id: parseInt(req.params.courseId)
    },
    data: {
      name: req.body.name,
      coursedetails: req.body.coursedetails
    }
  })
  res.status(200).json({
    course: updatedCourse
  })
}

exports.deleteCourse = async (req, res, next) => {
  await course.delete({
    where: {
      id: parseInt(req.params.courseId)
    }
  }, (err, response) => {
    if (err){
      res.status(500).json({
        message: "course could not be deleted"
      })
    }
    else{
      res.status(200).json({
        message: " course has been deleted."
      })
    }
  })
}

exports.createCoursesAndUsers = async (req, res, next) => {
 const newCourse = await course.create({
      data: {
        name: req.body.name,
        coursedetails: req.body.coursedetails,
        members: {
          create: [
            {
              role: 'TEACHER',
              users: {
                connect: req.body.teachers
              }
            }
          ]
        }
      }, 
      include: {
        members:{
          include: {
            users: true
          }
        }
      }
 })
 if (newCourse){
  res.status(200).json({
    status: "success",
    data: newCourse
  })
 }
 else{
  res.status(500).json({
    status: "failed",
    message: "Invalid user input."
  })
 }

}



//// TESTS RELATED
exports.updateTestResult = async (req,res, next) =>{
  const theGreader = await user.findUnique({
    where: {
      id: req.user.id
    }

  });

const newTestResult = await testResult.create({
  data: {
      graded: true,
      result: parseInt(req.body.result),
      test: {
          connect: {
              id: parseInt(req.params.testId)
          }
      },
      student:{
          connect:{
              email: req.body.student.email
          }
      },
      grader:{
          connect: {
              email: theGreader.email
          }
      }
  },
  include:{
      test: true,
      student: true,
      grader: true
  }
})

if(!newTestResult){
  res.status(201).json({
      message: "Unable to update test result."
  })
}
else{
  res.status(200).json({
      message: "created for the specified student and teacher",
      testResult: newTestResult
  })
}
}
exports.createTestResult = async (req, res, nex) =>{
  let isTeacher= false;
  let theCourse;
  const theGreader = await user.findUnique({
          where: {
            id: req.user.id
          }
        });
        console.log(theGreader)
    const theStudent = await user.findUnique({
      where: {
        email: req.body.email
      }
    })
      if(!theStudent){
        res.status(404).json({
          message: "the student has not been registered."
        })
      }
      else {
        const teachingCourses = await course.findMany({
          where: {
            members:{
              every:{
                userId: req.user.id
              }
             }
            }
        })
        const tests = await test.findMany({
          where:{
            id: parseInt(req.params.testId)
          },
          select:{
            courseId: true
          }
        })
        if(!tests){
          res.status(404).json({
            message: "there is no test with the specified id."
          })
        }
        else{
        teachingCourses.forEach(element => {
          if(element.id == parseInt(tests[0].courseId))
              {
                isTeacher = true
                
              console.log(element)
            }
        });
      }
       if(isTeacher){
       const newTestResult = await testResult.create({
         data: {
            graded: true,
            result: parseInt(req.body.result),
            test: {
                connect: {
                    id: parseInt(req.params.testId)
                }
            },
            student:{
                connect:{
                    email: req.body.email
                }
            },
            grader:{
                connect: {
                    email: theGreader.email
                }
            }
        },
        include:{
            test: true,
            student: true,
            grader: true
        }
    })
    await courseEnrollment.create({
      data:{
        userId: theStudent.id,
        role: UserRole.STUDENT,
        courseId: tests[0].courseId
      }
    })
    /*await user.update({
      where:{
        id: theStudent.id
      },
      data:{
        courses:{
          create:{
            role: UserRole.STUDENT
          },
          connect:{
            courseId: newTestResult.test.courseId
          }
        }
      },
      include:{
        courses:true
      }
    })
    //console.log(apdated);
*/
    if(!newTestResult){
        res.status(500).json({
            message: "Unable to create test result."
        })
    }
    else{
       
        res.status(200).json({
            message: "created for the specified student and teacher",
            testResult: newTestResult
        })
    }
  }
  else{
    res.status(403).json({
      message: "This is not one of the tests of the courses that you teach"
  })
  }
      }
  
}
exports.viewAllTestResults = async (req, res, next) =>{
  const allTesResults = await testResult.findMany({
    include: {
      test:true,
      grader: true,
      student: true
    }
  })
  if(!allTesResults){
    res.status(500).json({
      message: "Could not fetch the test results"
    })
  }
  else{
    res.status(200).json({
      data: allTesResults
    })
  }
}
exports.createTest = async (req, res, next)=>{
let isTeacher = false;
   const teachingCourses = await user.findMany({
    where: {
      id: req.user.id
    },
    include:{
      courses: true
    }
   })
   console.log(teachingCourses[0].courses)
   if (!teachingCourses){
    res.status(204).json({
      message: " No courses teaching",
      
    }) 
  }
  else{
   teachingCourses[0].courses.forEach(element => {
    if(element.courseId == req.params.courseId){
      console.log(element)
      isTeacher = true
    }
   })
  }
  if(isTeacher){
  
  const createdTest = await test.create({
    data:{
      name: req.body.name,
      courseId: parseInt(req.params.courseId)
    }
  })
  res.status(200).json({
    message: "Test created.",
    test: createdTest
  }) 
}else{
  res.status(403).json({
    message: "This is not one of the courses you teach.",
    
  }) 
}
}
exports.viewAllTests = async (req, res, next) =>{
  const alltests = await test.findMany({
    include:{
      courses: true,
    }
  })
  res.status(200).json({
    message: " ALl tests.",
    tests: alltests
  })
}

//exports.submitTestResult = asnyc (req, res, )
